// ====================================================================
// Allan CORNET 
// Copyright DIGITEO 2011
//
// Antoine ELIAS
// Copyright Scilab Enterprises 2013
// ====================================================================

mode(-1);
lines(0);

function main_builder()

  TOOLBOX_NAME  = "automation";
  TOOLBOX_TITLE = "automation";
  toolbox_dir   = get_absolute_file_path("builder.sce");
// =============================================================================
// Check Scilab's version
// =============================================================================
try
  v = getversion("scilab");
catch
  error(gettext("Scilab 6.0 or more is required."));
end

if v(1) < 6 then
  error(gettext('Scilab 6.0 or more is required.'));
end
clear v;
// =============================================================================
// Action
// =============================================================================
if getos() == 'Windows' then
    setenv('SCIDir', SCI + filesep());

    compiler = findmsvccompiler();
    if strstr(compiler, "express") <> "" then
        //compilerbin = 'C:\Program Files (x86)\Microsoft Visual Studio\ 10.0\Common7\IDE\VCExpress.exe ';
        compilerbin = '""C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.exe"" ';
    else
        compilerbin = '""C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.exe"" ';
    end
    
    if win64() then
        if (getenv('DEBUG_SCILAB_DYNAMIC_LINK','NO') == 'YES') then
            cmdline = compilerbin + toolbox_dir + 'automation.sln /Build ""Debug|x64""';
        else
            cmdline = compilerbin + toolbox_dir + 'automation.sln /Build ""Release|x64""';
        end
    else
        if (getenv('DEBUG_SCILAB_DYNAMIC_LINK','NO') == 'YES') then
            cmdline = compilerbin + toolbox_dir + 'automation.sln /Build ""Debug|Win32""';
        else
            cmdline = compilerbin + toolbox_dir + 'automation.sln /Build ""Release|Win32""';
        end
    end

    disp(cmdline);
    [output, bOK, exitcode] = dos(cmdline);
    if (ilib_verbose() > 1 | ~bOK) then
        disp(output);
    end

    clear output;
    clear bOK;
    clear exitcode;
    clear cmdline;

    clear findmsvccompiler;
    tbx_builder_macros(toolbox_dir);
    //tbx_builder_help(toolbox_dir);
else
    warning("Only for Windows Environment.");
end

endfunction
// =============================================================================
main_builder();
clear main_builder; // remove main_builder on stack
// =============================================================================


