/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <string.h>
#include "createMListVariant.h"
#include "charEncoding.h"
#include "sci_malloc.h"
#include <limits>
/*--------------------------------------------------------------------------*/
static char ** matrixWideStringToString(int _iRows, int _iCols, const wchar_t **_pstStrings, int *iError);
/*--------------------------------------------------------------------------*/
SciErr createMListVariant(void* _pvCtx,
    int _iVar,
    int _iRows, int _iCols,
    const wchar_t **_pwstStrings,
    const double* _pdblReal)
{
    SciErr sciErr;
	void* pvApiCtx = _pvCtx;
    sciErr.iErr = 0;
    static char* fields[] = {"VARIANT", "string", "value"};
    int iNbItem = 3; // "VARIANT", "string", "value"
    int *pmListAddr = NULL;

    char **pStrings = NULL;

    sciErr = createMList(_pvCtx, _iVar, iNbItem,  &pmListAddr);
    if(sciErr.iErr)
    {
        return sciErr;
    }
    sciErr = createMatrixOfStringInList(_pvCtx, _iVar, pmListAddr, 1, 1, iNbItem , fields);
    if(sciErr.iErr)
    {
        return sciErr;
    }

    int iErr = 0;
    if (_pwstStrings)
    {
        pStrings = matrixWideStringToString(_iRows, _iCols, _pwstStrings, &iErr);
    }
    else
    {
        pStrings = new char *[ _iRows * _iCols];
        if (pStrings)
        {
            for (int i = 1; i <  _iRows * _iCols; i++)
            {
                pStrings[i] = _strdup("");
            }
        }

    }

    if (iErr == 0)
    {
        sciErr = createMatrixOfStringInList(pvApiCtx, _iVar, pmListAddr, 2, _iRows, _iCols, pStrings);
    }

    if (pStrings)
    {
        for (int i = 1; i <  _iRows * _iCols; i++)
        {
            delete pStrings[i];
            pStrings[i] = NULL;
        }
        delete pStrings;
        pStrings = NULL;
    }

    if (sciErr.iErr == 0)
    {
        if ( _pdblReal)
        {
            sciErr = createMatrixOfDoubleInList(pvApiCtx, _iVar, pmListAddr, 3, _iRows, _iCols, _pdblReal);
        }
        else
        {
            double *pDbls = new double[_iCols * _iRows];
            if (pDbls)
            {
                for (int i = 0; _iCols * _iCols; i++)
                {
                    pDbls[i] = std::numeric_limits<double>::quiet_NaN();
                }

                sciErr = createMatrixOfDoubleInList(pvApiCtx, _iVar, pmListAddr, 3, _iRows, _iCols, pDbls);
                delete [] pDbls;
                pDbls = NULL;
            }
            else
            {
                sciErr.iErr = API_ERROR_ALLOC_DOUBLE;
            }
        }
    }
    return sciErr;
}
/*--------------------------------------------------------------------------*/
static char ** matrixWideStringToString(int _iRows, int _iCols, const wchar_t **_pstStrings, int *iError)
{
    char **pstrReturned = NULL;
    *iError = 1;
    if ( (_iCols == 0) && (_iRows == 0) && (_pstStrings == NULL) )
    {
        *iError = 0;
    }
    else if (_pstStrings)
    {
        int nbElements = _iCols * _iRows;
        pstrReturned = new char *[nbElements];
        if (pstrReturned)
        {
            for (int i = 0; i < nbElements; i++)
            {
                if (_pstStrings[i])
                {
                    char *pSTR = wide_string_to_UTF8(_pstStrings[i]);
                    if (pSTR)
                    {
                        pstrReturned[i] = _strdup(pSTR);
                        FREE(pSTR);
                        pSTR = NULL;
                    }
                    else
                    {
                        pstrReturned[i] = NULL;
                    }
                }
                else
                {
                    pstrReturned[i] = NULL;
                }
            }
            *iError = 0;
        }
    }
    return pstrReturned;
}
/*--------------------------------------------------------------------------*/
