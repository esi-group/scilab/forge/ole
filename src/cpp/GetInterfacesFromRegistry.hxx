/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#ifndef __GETINTERFACESFROMREGISTRY_HXX__
#define __GETINTERFACESFROMREGISTRY_HXX__

struct COM_INTERFACE_INFO
{
    wchar_t **wName;
    wchar_t **wProgID;
    wchar_t **wFile;
    int nbElements;
};

COM_INTERFACE_INFO *GetInterfacesFromRegistry(int *iError);
bool deleteInterfacesInfo(COM_INTERFACE_INFO * infos);

#endif /* __GETINTERFACESFROMREGISTRY_HXX__ */
/*--------------------------------------------------------------------------*/
