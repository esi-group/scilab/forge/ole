/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/*--------------------------------------------------------------------------*/
#include <vector>
#include "VariantsManager.hxx"
/*--------------------------------------------------------------------------*/
static std::vector<VARIANT *> variantVector;
static int getPositionVariantInManager(VARIANT *pVariant);
/*--------------------------------------------------------------------------*/
bool addVariantInManager(VARIANT *pVariant)
{
	if (pVariant)
	{
		if (!findVariantInManager(pVariant))
		{
			variantVector.push_back(pVariant);
		}
		return true;
	}
	return false;
}
/*--------------------------------------------------------------------------*/
bool removeVariantInManager(VARIANT *pVariant)
{
	if (pVariant)
	{
		int pos = getPositionVariantInManager(pVariant);
		if (pos > -1) 
		{
			variantVector.erase(variantVector.begin() + pos);
			return true;
		}
	}
	return false;
}
/*--------------------------------------------------------------------------*/
bool findVariantInManager(VARIANT *pVariant)
{
	if (!variantVector.empty())
	{
		int pos = getPositionVariantInManager(pVariant);
		if (pos > -1) return true;
	}
	return false;
}
/*--------------------------------------------------------------------------*/
int getPositionVariantInManager(VARIANT *pVariant)
{
	int pos = -1;
	if (pVariant)
	{
		for (size_t i = 0; i < variantVector.size(); i++)
		{
			if (variantVector[i] == pVariant) return (int)i;
		}
	}
	return pos;
}
/*--------------------------------------------------------------------------*/
VARIANT *getLastVariantInManager(void)
{
    VARIANT *pVariant = NULL;
    if (variantVector.size() > 0)
    {
        pVariant = variantVector[variantVector.size() - 1];
    }
    return pVariant;
}
/*--------------------------------------------------------------------------*/
