/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/*--------------------------------------------------------------------------*/
#ifndef __VARIANTSMANAGER_HXX__
#define __VARIANTSMANAGER_HXX__
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
/*--------------------------------------------------------------------------*/
bool addVariantInManager(VARIANT *pVariant);
bool removeVariantInManager(VARIANT *pVariant);
bool findVariantInManager(VARIANT *pVariant);
VARIANT *getLastVariantInManager(void);
/*--------------------------------------------------------------------------*/
#endif /* __VARIANTSMANAGER_HXX__ */
/*--------------------------------------------------------------------------*/ 
