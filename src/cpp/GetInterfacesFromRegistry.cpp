/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <windows.h>
#include "sciprint.h"
#include "GetInterfacesFromRegistry.hxx"
/*--------------------------------------------------------------------------*/
static wchar_t* reg_enum_key(HKEY hkey, DWORD i);
/*--------------------------------------------------------------------------*/
COM_INTERFACE_INFO *GetInterfacesFromRegistry(int *iError)
{
    COM_INTERFACE_INFO *pInterfaces = NULL;
    *iError = 1;

    pInterfaces = new COM_INTERFACE_INFO;
    if (pInterfaces)
    {
        //http://www.mathworks.com/help/techdoc/matlab_external/f64299.html

        HKEY hclsid;
        LONG err = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\CLASSES\\CLSID", 0, KEY_READ, &hclsid);
        BOOL found = FALSE;

        if (err != ERROR_SUCCESS)
        {
             RegCloseKey(hclsid);
            *iError = 1;
            return NULL;
        }

        for(int i = 0; !found; i++)
        {
            wchar_t *clsidString = reg_enum_key(hclsid, i);
            if (clsidString)
            {
                if (wcscmp(clsidString, L"CLSID") != 0)
                {
                    /* start */
                    HKEY hKey;
                    DWORD dwNumValues, dwMaxValueNameLen, dwMaxValueLen,
                        index, dwInitValueNameLen, dwInitValueLen;
                    LPSTR pszValueName;
                    LPBYTE pData;

                    wchar_t subKey[1024];
                    wcscpy(subKey, L"SOFTWARE\\CLASSES\\CLSID\\");
                    wcscat(subKey, clsidString);
                    wcscat(subKey, L"\\VersionIndependentProgID");

                    if (wcscmp(L"{66288DCF-057B-4557-A2C4-E1D2EDBF67DB}", clsidString)==0)
                    {
                        MessageBox(NULL,NULL,NULL,MB_OK);
                    }

                    /* open the registry key */
                    if (SUCCEEDED(RegOpenKeyExW(HKEY_LOCAL_MACHINE,subKey, 0, KEY_QUERY_VALUE, &hKey)))
                    {
                        /* get info about what's in the key */
                        if (SUCCEEDED(RegQueryInfoKey (hKey, NULL, NULL, NULL, NULL, NULL,
                            NULL, &dwNumValues, &dwMaxValueNameLen, &dwMaxValueLen, NULL, NULL)))
                        {
                            /* allocate buffers that are big enough */
                            pszValueName = (LPSTR) calloc(1 + dwMaxValueNameLen,sizeof(char));
                            pData = (LPBYTE) calloc(1 + dwMaxValueLen,sizeof(char));

                            /* get the info in the registry key */
                            for (index = 0; index < dwNumValues; index++) 
                            {
                                dwInitValueNameLen = 1 + dwMaxValueNameLen;
                                dwInitValueLen = 1 + dwMaxValueLen;
                                if (SUCCEEDED(RegEnumValue(hKey, index, pszValueName,
                                    &dwInitValueNameLen, NULL, NULL, pData, &dwInitValueLen)))
                                {
                                    if (pData)
                                    {
                                        if (strlen((char*)pData) > 0)
                                        {
                                            sciprint("%s\n",pData);
                                        }
                                    }
                                }
                           }

                            /* clean up */
                            free(pszValueName);
                            free(pData);
                        }
                   }

                    RegCloseKey(hKey);
                    /* end */
                }
            }
            else
            {
                 break;
            }
        }

        RegCloseKey(hclsid);
    }
    return pInterfaces;
}
/*--------------------------------------------------------------------------*/
bool deleteInterfacesInfo(COM_INTERFACE_INFO * infos)
{
    if (infos)
    {
        int nbElements = infos->nbElements;
        for (int i = 0; i < nbElements; i++)
        {
            if (infos->wFile)
            {
                if (infos->wFile[i])
                {
                    delete infos->wFile[i];
                    infos->wFile[i] = NULL;
                }
            }

            if (infos->wProgID)
            {
                if (infos->wProgID[i])
                {
                    delete infos->wProgID[i];
                    infos->wProgID[i] = NULL;
                }
            }

            if (infos->wName)
            {
                if (infos->wName[i])
                {
                    delete infos->wName[i];
                    infos->wName[i] = NULL;
                }
            }
        }

        if (infos->wFile)
        {
            delete [] infos->wFile;
            infos->wFile = NULL;
        }

        if (infos->wProgID)
        {
            delete [] infos->wProgID;
            infos->wProgID = NULL;
        }

        if (infos->wName)
        {
            delete [] infos->wName;
            infos->wName = NULL;
        }

        infos->nbElements = 0;
        return true;
    }
    return false;
}
/*--------------------------------------------------------------------------*/
static wchar_t* reg_enum_key(HKEY hkey, DWORD i)
{
    wchar_t buf[1024];
    DWORD size_buf = sizeof(buf);
    FILETIME ft;
    LSTATUS err = RegEnumKeyExW(hkey, i, buf, &size_buf, NULL, NULL, NULL, &ft);
    if(err == ERROR_SUCCESS) 
    {
        return _wcsdup(buf);
    }
    return NULL;
}
/*--------------------------------------------------------------------------*/

