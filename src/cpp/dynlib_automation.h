
#ifndef __DYNLIB_AUTOMATION_H__
#define __DYNLIB_AUTOMATION_H__

#ifdef _MSC_VER
#ifdef AUTOMATION_EXPORTS
#define IMPORT_EXPORT_AUTOMATION_DLL __declspec(dllexport)
#else
#define IMPORT_EXPORT_AUTOMATION_DLL __declspec(dllimport)
#endif
#else
#define IMPORT_EXPORT_AUTOMATION_DLL
#endif

#endif /* __DYNLIB_AUTOMATION_H__ */
