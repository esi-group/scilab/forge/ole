// ============================================================================
// Allan CORNET - DIGITEO - 2011
// ============================================================================
function %ptr_p(ptr_value)
 if ole_iscom(ptr_value) then
   if ole_getVariantType(ptr_value) == "VT_DISPATCH" then
     disp("COM." + ole_classInfo(ptr_value));
   else
     disp(ole_getVariantType(ptr_value));
   end
 end
endfunction
// ============================================================================
