// ============================================================================
// Allan CORNET - DIGITEO - 2011
// ============================================================================
function OLEpath = ole_getOlePath()
  [fs, OLEpath] = libraryinfo("automationlib");
  OLEpath = pathconvert(fullpath(OLEpath + "../"), %t, %t);
endfunction
// ============================================================================