// ============================================================================
// Allan CORNET - DIGITEO - 2011
// ============================================================================
function %VARIANT_p(VARIANT)
  //display a safearray VARIANT
  TEXT_VARIANT = VARIANT.string;
  VALUE_VARIANT = VARIANT.value;
  POS_VALUE = find(~isnan(VALUE_VARIANT));
  TEXT_VARIANT(POS_VALUE) = string(VALUE_VARIANT(POS_VALUE));
  disp(TEXT_VARIANT);
endfunction
// ============================================================================
