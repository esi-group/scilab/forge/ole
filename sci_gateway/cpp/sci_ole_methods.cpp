/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/*--------------------------------------------------------------------------*/
#define _WIN32_WINNT 0x0501
#define _WIN32_IE 0x0501
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "AutomationHelper.hxx"
#include "VariantHelper.hxx"
#include "Scierror.h"
#include "localization.h"
/*--------------------------------------------------------------------------*/
int sci_ole_methods(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
	void* pvPtr = NULL;
	VARIANT *pVariantDisp = NULL;
	wchar_t **names = NULL;
	int nbNames = 0;

	CheckRhs(1, 1);
	CheckLhs(0, 1);

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

    if (!isPointerType(pvApiCtx, piAddrOne))
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: pointer expected.\n"),fname, 1);
        return 0;
    }

	sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (!isVariant(pvPtr))
	{
		Scierror(999, "%s: Invalid pointer: It is not a VARIANT.\n", fname);
		return 0;
	}

	pVariantDisp = (VARIANT *)pvPtr;
	if (pVariantDisp->vt != VT_DISPATCH)
	{
		Scierror(999, "%s: Invalid VARIANT type.\n", fname);
		return 0;
	}

	names = getMethodsName(pVariantDisp->pdispVal, &nbNames);
	if ((names) && (nbNames > 0))
	{
		sciErr = createMatrixOfWideString(pvApiCtx, Rhs + 1, nbNames, 1, names);

		for (int i = 0; i < nbNames; i++)
		{
			if (names[i])
			{
				delete names[i];
				names[i] = NULL;
			}
		}
		delete [] names;
		names = NULL;
		nbNames = 0;

		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}
	}
	else
	{
		sciErr = createMatrixOfDouble(pvApiCtx, Rhs + 1, 0, 0, NULL);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}
	}
	LhsVar(1) = Rhs + 1;	
	PutLhsVar();
	return 0;
}
/*--------------------------------------------------------------------------*/
