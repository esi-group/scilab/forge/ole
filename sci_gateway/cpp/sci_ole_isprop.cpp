/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "Scierror.h"
#include "localization.h"
#include "VariantHelper.hxx"
#include "AutomationHelper.hxx"
/*--------------------------------------------------------------------------*/
int sci_ole_isprop(char *fname, void* pvApiCtx)
{
    SciErr sciErr;
    int* piAddrOne = NULL;
    int* piAddrTwo = NULL;
    BOOL bIsProperty = FALSE;

    CheckRhs(2, 2);
    CheckLhs(0, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddrTwo);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (isScalar(pvApiCtx, piAddrTwo) == 0)
    {
        Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, 2);
        return 0;
    }

    if (isPointerType(pvApiCtx, piAddrOne))
    {
        void* pvPtr = NULL;
        sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (isVariant(pvPtr))
        {
            wchar_t *propertyName = NULL;
            VARIANT *pVariantDisp = (VARIANT *)pvPtr;

            if (pVariantDisp->vt != VT_DISPATCH)
            {
                Scierror(999, "%s: Invalid VARIANT type.\n", fname);
                return 0;
            }

            if (getAllocatedSingleWideString(pvApiCtx, piAddrTwo, &propertyName) != 0)
            {
                Scierror(999,_("%s: No more memory.\n"), fname);
                return 0;
            }

            bIsProperty = ole_isPropertyGet(pVariantDisp->pdispVal, propertyName) || 
                          ole_isPropertyPut(pVariantDisp->pdispVal, propertyName);

            freeAllocatedSingleWideString(propertyName);
            propertyName = NULL;
        }
    }

    if (createScalarBoolean(pvApiCtx, Rhs + 1, bIsProperty) == 0)
    {
        LhsVar(1) = Rhs + 1;	
        PutLhsVar();
    }
    else
    {
        Scierror(999,_("%s: No more memory.\n"),fname);
    }
    return 0;
}
/*--------------------------------------------------------------------------*/
