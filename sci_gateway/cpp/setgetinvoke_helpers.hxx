/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#ifndef __SETGETINVOKE_HXX__
#define __SETGETINVOKE_HXX__

#include <Ole2.h>

#include "AutomationHelper.hxx"

#define firstArgPosToCheck 3
#define NBARGSINMAX 10

int sgi_checkTypeOptionalInputArguments(char *fname, void *pvApiCtx);
int sgi_checkSizeOptionalInputArguments(char *fname, void *pvApiCtx);

VARIANT * sgi_getVariantsOptionalInputArguments(char *fname, void *pvApiCtx, int *ierror, int *nbArgumentsReturned);

char *getInvokeErrorMessage(INVOKE_ERROR InvokeError);

int variantToScilabTypeOnStack(const char *fname, void *pvApiCtx, int stackPosition, VARIANT *pVariant);

#endif /* __SETGETINVOKE_HXX__ */
/*--------------------------------------------------------------------------*/

