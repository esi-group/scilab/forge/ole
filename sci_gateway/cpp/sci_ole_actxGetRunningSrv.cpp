/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "VariantsManager.hxx"
#include "Scierror.h"
#include "localization.h"
/*--------------------------------------------------------------------------*/
int sci_ole_actxGetRunningSrv(char *fname, void* pvApiCtx)
{
    IUnknown *pUnknown;
    IDispatch * pdispApplication = NULL;
    VARIANT *pVariantApplication = NULL;
    CLSID clsApplication;
    HRESULT hRes = S_FALSE;
    wchar_t *pStrApplicationName = NULL;
    SciErr sciErr;
    int* piAddr = NULL;

    CheckRhs(1, 1);
    CheckLhs(0, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (isScalar(pvApiCtx, piAddr) == 0)
    {
        Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, 1);
        return 0;
    }

    if (getAllocatedSingleWideString(pvApiCtx, piAddr, &pStrApplicationName) != 0)
    {
        Scierror(999,_("%s: No more memory.\n"), fname);
        return 0;
    }

	if (pStrApplicationName[0] == L'{')
	{
		if (FAILED(CLSIDFromString(pStrApplicationName, &clsApplication))) 
		{
			freeAllocatedSingleWideString(pStrApplicationName);
			pStrApplicationName = NULL;
			Scierror(999,_("%s: Error Invalid PROGID.\n"),fname);
			return 0;
		}
	}
	else
	{
		if (FAILED(CLSIDFromProgID(pStrApplicationName, &clsApplication))) 
		{
			freeAllocatedSingleWideString(pStrApplicationName);
			pStrApplicationName = NULL;
			Scierror(999,_("%s: Error Invalid PROGID.\n"),fname);
			return 0;
		}
	}
 
    freeAllocatedSingleWideString(pStrApplicationName);
    pStrApplicationName = NULL;

    hRes = GetActiveObject(clsApplication, NULL, &pUnknown);
    if (FAILED(hRes))
    {
        Scierror(999,_("%s: Error Server is not running on this system.\n"),fname);
        return 0;
    }

    hRes = pUnknown->QueryInterface(IID_IDispatch, (void **)&pdispApplication);
    pUnknown->Release();

    if (FAILED(hRes))
    {
        Scierror(999,_("%s: Error Failed to connect to server.\n"),fname);
        return 0;
    }

    pVariantApplication = new VARIANT;
    VariantInit(pVariantApplication);
    pVariantApplication->vt = VT_DISPATCH;
    pVariantApplication->pdispVal = pdispApplication;

    addVariantInManager(pVariantApplication);
    sciErr = createPointer(pvApiCtx, Rhs + 1, (void*)pVariantApplication);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }
    LhsVar(1) = Rhs + 1;	
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
