/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include <ocidl.h>
#include "api_scilab.h"
#include "gw_automation.h"
#include "Scierror.h"
#include "localization.h"
#include "VariantHelper.hxx"
/*--------------------------------------------------------------------------*/
static int getClassInfoFromVariant(VARIANT *pVariant, wchar_t **className, wchar_t **classTypeName);
/*--------------------------------------------------------------------------*/
int sci_ole_classInfo(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;

	CheckRhs(1, 1);
	CheckLhs(1, 2);

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (isPointerType(pvApiCtx, piAddrOne))
	{
        void* pvPtr = NULL;
		sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		if (isVariant(pvPtr))
		{
            VARIANT *pVariantDisp =  (VARIANT *)pvPtr;
            if (pVariantDisp->vt != VT_DISPATCH)
            {
                Scierror(999, "%s: Invalid VARIANT type.\n", fname);
                return 0;
            }
            else
            {
                wchar_t *className = NULL;
                wchar_t *classTypeName = NULL;

                getClassInfoFromVariant(pVariantDisp, &className, &classTypeName);

                if (Lhs == 2)
                {
                    createSingleWideString(pvApiCtx, Rhs + 1, className);
                    createSingleWideString(pvApiCtx, Rhs + 2, classTypeName);

                    LhsVar(2) = Rhs + 2;	
                    LhsVar(1) = Rhs + 1;	
                }
                else
                {
                    wchar_t * wConcatStr = NULL;
                    wConcatStr = new wchar_t[wcslen(className) + wcslen(classTypeName) + wcslen(L"_") + 1];
                    if (wConcatStr)
                    {
                        wcscpy(wConcatStr, className);
                        wcscat(wConcatStr, L"_");
                        wcscat(wConcatStr, classTypeName);
                    }

                    createSingleWideString(pvApiCtx, Rhs + 1 , wConcatStr);
                    LhsVar(1) = Rhs + 1;	
                    
                    if (wConcatStr)
                    {
                        delete wConcatStr;
                        wConcatStr = NULL;
                    }
                }

                if (className)
                {
                    delete className;
                    className = NULL;
                }
                if (classTypeName)
                {
                    delete classTypeName;
                    classTypeName = NULL;
                }

                PutLhsVar();
            }
		}
        else
        {
            Scierror(999, "%s: Invalid pointer: It is not a COM Object.\n", fname);
        }
	}
	return 0;
}
/*--------------------------------------------------------------------------*/
static int getClassInfoFromVariant(VARIANT *pVariant, wchar_t **className, wchar_t **classTypeName)
{
    int iError = 1;
    IProvideClassInfo *ci = NULL;
    ITypeInfo *ti = NULL;
    unsigned int tiCount = 0;
    HRESULT hr;

    if (pVariant->pdispVal->QueryInterface(IID_IProvideClassInfo, (void**)&ci) == S_OK)
    {
        ci->GetClassInfo(&ti);
        ci->Release();
    }

    if (ti == NULL && (hr = pVariant->pdispVal->GetTypeInfoCount(&tiCount)) == S_OK && tiCount == 1)
    {
        pVariant->pdispVal->GetTypeInfo(0, LOCALE_USER_DEFAULT, &ti);
    }

    if (ti != NULL)
    {
        ITypeLib *pTypeLib = NULL;
        BSTR bSTRclassName;
        BSTR bSTRClassType;

        if (SUCCEEDED(ti->GetContainingTypeLib(&pTypeLib, 0)))
        {
            if (SUCCEEDED(pTypeLib->GetDocumentation(MEMBERID_NIL, &bSTRclassName, NULL, NULL, NULL)))
            {
                *className = _wcsdup(bSTRclassName);
                SysFreeString(bSTRclassName);
            }
            else
            {
                *className = _wcsdup(L"Unknown");
            }
        }
        else
        {
            *className = _wcsdup(L"Unknown");
        }

        if (SUCCEEDED(ti->GetDocumentation(MEMBERID_NIL, & bSTRClassType, NULL, NULL, NULL)))
        {
            *classTypeName = _wcsdup(bSTRClassType);
            SysFreeString(bSTRClassType);
        }
        else
        {
            *classTypeName = _wcsdup(L"Unknown");
        }
        iError = 0;
    }
    else
    {
        *classTypeName = _wcsdup(L"_Unknown");
        *className = _wcsdup(L"Unknown");
    }
    ti->Release();
   return iError;
}
/*--------------------------------------------------------------------------*/
