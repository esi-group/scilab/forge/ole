/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2010 */
/*--------------------------------------------------------------------------*/

extern "C"
{
#include <wchar.h>
#include "gw_automation.h"
#include "api_scilab.h"
#include "sci_malloc.h"
#include "c_gateway_prototype.h"
#include "addfunction.h"


#define MODULE_NAME L"gw_automation"

#define SIZE_CURRENT_GENERIC_TABLE(tab) (sizeof(tab) / sizeof(gw_generic_table))

typedef int (*function_Interf)(char *fname, void* pvApiCtx);

typedef struct functions_table_struct {
	function_Interf f; /** the function itself **/
	char const * const name;      /** its name in Scilab **/
	/* char const * const , to remove some warnings -Wall (linux) */
} gw_generic_table;

extern void callFunctionFromGateway(gw_generic_table *Tab, int sizeTab);

}

/*--------------------------------------------------------------------------*/
static gw_generic_table Tab[] = {
    {sci_ole_actxserver, "ole_actxserver"},
    {sci_ole_actxGetRunningSrv, "actxGetRunningSrv"},
	{sci_ole_invoke, "ole_invoke"},
    {sci_ole_set, "ole_set"},
    {sci_ole_get, "ole_get"},
	{sci_ole_variantToScilab, "ole_variantToScilab"},
	{sci_ole_scilabToVariant, "ole_scilabToVariant"},
	{sci_ole_methods, "ole_methods"},
	{sci_ole_properties, "ole_properties"},
	{sci_ole_getVariantType, "ole_getVariantType"},
	{sci_ole_delete, "ole_delete"},
	{sci_ole_iscom, "ole_iscom"},
    {sci_ole_isprop, "ole_isprop"},
    {sci_ole_isprop, "ole_ismethod"},
    {sci_ole_variantChangeType, "ole_variantChangeType"},
    {sci_ole_actxsrvlist, "ole_actxsrvlist"},
    {sci_ole_mode, "ole_mode"},
    {sci_ole_classInfo, "ole_classInfo"},
};
/*--------------------------------------------------------------------------*/ 

int gw_automation(wchar_t *pwstFuncName) {

	if (wcscmp(pwstFuncName, L"ole_actxserver") == 0) { addCStackFunction(L"ole_actxserver", &sci_ole_actxserver, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"actxGetRunningSrv") == 0) { addCStackFunction(L"actxGetRunningSrv", &sci_ole_actxGetRunningSrv, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_invoke") == 0) { addCStackFunction(L"ole_invoke", &sci_ole_invoke, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_set") == 0) { addCStackFunction(L"ole_set", &sci_ole_set, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_get") == 0) { addCStackFunction(L"ole_get", &sci_ole_get, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_variantToScilab") == 0) { addCStackFunction(L"ole_variantToScilab", &sci_ole_variantToScilab, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_scilabToVariant") == 0) { addCStackFunction(L"ole_scilabToVariant", &sci_ole_scilabToVariant, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_methods") == 0) { addCStackFunction(L"ole_methods", &sci_ole_methods, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_properties") == 0) { addCStackFunction(L"ole_properties", &sci_ole_properties, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_getVariantType") == 0) { addCStackFunction(L"ole_getVariantType", &sci_ole_getVariantType, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_delete") == 0) { addCStackFunction(L"ole_delete", &sci_ole_delete, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_iscom") == 0) { addCStackFunction(L"ole_iscom", &sci_ole_iscom, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_isprop") == 0) { addCStackFunction(L"ole_isprop", &sci_ole_isprop, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_ismethod") == 0) { addCStackFunction(L"ole_ismethod", &sci_ole_ismethod, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_variantChangeType") == 0) { addCStackFunction(L"ole_variantChangeType", &sci_ole_variantChangeType, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_actxsrvlist") == 0) { addCStackFunction((wchar_t *)L"ole_actxsrvlist", &sci_ole_actxsrvlist, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_mode") == 0) { addCStackFunction(L"ole_mode", &sci_ole_mode, MODULE_NAME); }
	if (wcscmp(pwstFuncName, L"ole_classInfo") == 0) { addCStackFunction(L"ole_classInfo", &sci_ole_classInfo, MODULE_NAME); }

	return 1;
}
/*--------------------------------------------------------------------------*/
