/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "VariantHelper.hxx"
#include "Scierror.h"
#include "localization.h"
/*--------------------------------------------------------------------------*/
int sci_ole_getVariantType(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
	void* pvPtr = NULL;
	VARIANT *pVariant = NULL;
	const wchar_t *variantName = NULL;

	CheckRhs(1, 1);
	CheckLhs(0, 1);

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

    if (!isPointerType(pvApiCtx, piAddrOne))
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: pointer expected.\n"),fname, 1);
        return 0;
    }

	sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (!isVariant(pvPtr))
	{
		Scierror(999, "%s: Invalid pointer: It is not a VARIANT.\n", fname);
		return 0;
	}

	pVariant = (VARIANT *)pvPtr;
	variantName = getVariantTypeAsString(pVariant);
	if (variantName)
	{
		if (createSingleWideString(pvApiCtx, Rhs + 1, variantName) == 0)
        {
		    LhsVar(1) = Rhs + 1;
		    PutLhsVar();
        }
        else
        {
            Scierror(999,_("%s: No more memory.\n"), fname);
        }
	}
    else
    {
        Scierror(999,_("%s: No more memory.\n"),fname);
    }
	return 0;
}
/*--------------------------------------------------------------------------*/
